package com.testng;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class ActTime_Testing {
	WebDriver driver;

	@BeforeMethod
	public void launchurl() {
		System.setProperty("webdriver.chrome.driver", "/home/ajay/chromedriver_linux64/chromedriver");
		driver = new ChromeDriver();
		driver.get("https://demo.actitime.com/login.do");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
	}

	@Test(priority = 1)
	public void gettext() throws InterruptedException {
        Thread.sleep(2000);
		String t = "Please identify yourself";
		if (driver.getPageSource().contains("Please identify")) {
			System.out.println("pass");
		} else {
			System.out.println("fail");
		}
	}

	@Test(priority = 2)
	public void logo() throws InterruptedException {
		Thread.sleep(2000);
		if (driver.findElement(By.xpath("//div[@class='atLogoImg']")).isDisplayed()) {
			System.out.println("pass");

		} else {

			System.out.println("fail");
		}
	}

	@AfterMethod
	public void close() {
		driver.quit();
	}

}
