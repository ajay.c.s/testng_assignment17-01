package com.testng;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Cellphone_Testng {
	WebDriver driver;

	@BeforeClass
	public void before() {
		System.setProperty("webdriver.chrome.driver", "/home/ajay/chromedriver_linux64/chromedriver");
		driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	@Test(priority = 1)
	public void login() {

		driver.findElement(By.xpath("//a[contains(text(),'Log in')]")).click();
		driver.findElement(By.id("Email")).sendKeys("ajaychandru12@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("Ajay@1999");
		driver.findElement(By.xpath("//input[@value='Log in']")).click();

	}

	@Test(priority = 2)
	public void product() throws InterruptedException {

		WebElement cellphone = driver.findElement(By.xpath("//a[contains(text(),'Electronics')][1]"));
		Actions act = new Actions(driver);
		act.moveToElement(cellphone).build().perform();
		act.moveToElement(driver.findElement(By.xpath("//a[contains(text(),'Cell phones')][1]"))).click().perform();
		driver.findElement(By.id("products-orderby")).click();
		Thread.sleep(2000);
		WebElement element = driver.findElement(By.id("products-orderby"));
		Select sel = new Select(element);
		sel.selectByValue("https://demowebshop.tricentis.com/cell-phones?orderby=10");
		Thread.sleep(2000);
	}

	@Test(priority = 3)
	public void addtocart() throws InterruptedException {
		driver.findElement(By.xpath("//input[@value='Add to cart'][1]")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("add-to-cart-button-80")).click();
		driver.findElement(By.linkText("Shopping cart")).click();
	}

	@Test(priority = 4)
	public void checkout() {
		driver.findElement(By.id("termsofservice")).click();
		driver.findElement(By.id("checkout")).click();

	}

	@Test(priority = 5)
	public void confirmorder() throws InterruptedException {
		driver.findElement(By.xpath("//div[@id='billing-buttons-container']//input[@value='Continue']")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("PickUpInStore")).click();
		driver.findElement(By.xpath("(//input[@value='Continue'])[2]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//input[@value='Continue'])[4]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[@value='Continue'])[5]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@value='Confirm']")).click();
		Thread.sleep(2000);
	}

	@Test(priority = 6)
	public void ordersucces() throws InterruptedException {
		WebElement thank = driver.findElement(By.xpath("//h1[contains(text(),'Thank you')]"));
		String thanks = thank.getText();
		System.out.println(thanks);
		WebElement order = driver
				.findElement(By.xpath("//strong[contains(text(),'Your order has been successfully processed!')]"));
		String orders = order.getText();
		System.out.println(orders);
		Thread.sleep(2000);
		WebElement id = driver.findElement(By.xpath("//ul[@class='details']"));
		String ids = id.getText();
		System.out.println(ids);
		driver.findElement(By.xpath("//input[@value='Continue']")).click();
	}

	@AfterClass
	public void quit() {
		driver.quit();
	}
}
