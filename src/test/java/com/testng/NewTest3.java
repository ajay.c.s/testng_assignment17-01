package com.testng;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class NewTest3 {
  @Test(groups={"Smoke Test"})
  public void createCustomer() {
	  System.out.println("Customer is created");
  }
  @Test(groups={"Regression Test"})
  public void newCustomer() {
	  System.out.println("New Customer is created");
  }
  @Test(groups={"Usability Test"})
  public void modifyCustomer() {
	  System.out.println("Customer is modified");
  }
  @Test(groups={"Smoke Test"})
  public void changeCustomer() {
	  System.out.println("Customer is changed");
  }
  @BeforeClass
  public void beforeClass() {
	  System.out.println("start data connection,launch browser");
  }
  @AfterClass
  public void afterClass() {
	  System.out.println("close data connection,close browser");
  }
}
